<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function contact(Request $request)
    {
        $request->validate([
            'name'=> 'required',
            'email'=> 'required|email',
            'contact'=> 'required',
            'businessName'=> 'required',
        ]);

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME','One Corner'));
        $email->setSubject('Book a Demo');
        $email->addTo('contact@onecorner.com.au');
        
        $html = '<b>Name</b>: '.$request->name.'<br><b>Email</b>: '.$request->email.'<br><b>Contact No.</b>: '.$request->contact.'<br><b>Business Name</b>: '.$request->businessName;
        
        $email->addContent(
            "text/html", $html
        );

        $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
        try {
            $sendgrid->send($email);
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }

        $request->session()->flash('success', 'Thank you for your interest in One Corner, we will get back yo you as soon as possible.');
        return redirect()->back();   
    }

    public function apply(Request $request)
    {
        $request->validate([
            'first_name'=> 'required',
            'last_name'=> 'required',
            'email'=> 'required|email',
            'contact'=> 'required',
            'businessName'=> 'required',
            'service'=> 'required',
            'type'=> 'required',
            'website'=> 'required',
            'role'=> 'required',
            'source'=> 'required'
        ]);

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME','One Corner'));
        $email->setSubject('Venue Apply');
        $email->addTo('contact@onecorner.com.au');
        
        $html = '<b>Name:</b> <br>'.$request->first_name.' '.$request->last_name.'<br><br>';
        $html .= '<b>Email:</b> <br>'.$request->email.'<br><br>';
        $html .= '<b>Contact No.:</b> <br>'.$request->contact.'<br><br>';
        $html .= '<b>Business Name:</b> <br>'.$request->businessName.'<br><br>';
        $html .= '<b>Which suburb is your Business in?</b> <br>'.($request->suburb ? : '-').'<br><br>';
        $html .= '<b>What services will you provide?</b> <br>'.$request->service.'<br><br>';
        $html .= '<b>What describes your business the best?</b> <br>'.$request->type.'<br><br>';
        $html .= '<b>What is your Venue’s website (or social media links)?</b> <br>'.$request->website.'<br><br>';
        $html .= '<b>What is your role in the business?</b> <br>'.$request->role.'<br><br>';
        $html .= '<b>How did you know about One Corner?</b> <br>'.$request->source.'<br><br>';
        $html .= '<b>Are you currently using any other online ordering system?</b> <br>'.($request->current_system ? :'-').'<br><br>';
        
        $email->addContent(
            "text/html", $html
        );

        $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
        try {
            $sendgrid->send($email);
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }

        $request->session()->flash('success', 'Thank you for your interest, we will get back to you as soon as possible.');
        return redirect('/');   
    }
}
