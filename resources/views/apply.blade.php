@extends('layouts.app')

@section('body')
<div class="container" style="padding-top:10%">
    <h4 class="g-mb-20">Apply for online ordering system</h4>
    <p>Interested in getting your online ordering system, please fill up this quick form and we will be in contact with you ASAP.</p>
    <form class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30" method="POST" action="{{ route('apply') }}">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group g-mb-25">
                    <label >*First Name</label>
                    <input type="text"  name="first_name" class="form-control rounded-0 form-control-md"  placeholder="Enter First Name" value="{{ old('first_name') }}" required>
                    @if ($errors->has('first_name'))
                        <div class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group g-mb-25">
                    <label >*Last Name</label>
                    <input type="text"  name="last_name" class="form-control rounded-0 form-control-md"  placeholder="Enter Last Name" value="{{ old('last_name') }}" required>
                    @if ($errors->has('last_name'))
                        <div class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group g-mb-25">
                    <label >*Email</label>
                    <input type="email"  name="email" class="form-control rounded-0 form-control-md"  placeholder="Enter Email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <div class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group g-mb-25">
                    <label >*Mobile No</label>
                    <input type="text"  name="contact" class="form-control rounded-0 form-control-md"  placeholder="Enter Mobile Number" value="{{ old('contact') }}" required>
                    @if ($errors->has('contact'))
                        <div class="help-block">
                            <strong>{{ $errors->first('contact') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group g-mb-25">
                    <label >*Name of your Business</label>
                    <input type="text"  name="businessName" class="form-control rounded-0 form-control-md" placeholder="Enter business name" value="{{ old('businessName') }}" required>
                    @if ($errors->has('businessName'))
                        <div class="help-block">
                            <strong>{{ $errors->first('businessName') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group g-mb-25">
                    <label>Which suburb is your Business in?</label>
                    <input type="text"  name="suburb" class="form-control rounded-0 form-control-md" placeholder="Enter business location" value="{{ old('suburb') }}">
                    @if ($errors->has('suburb'))
                        <div class="help-block">
                            <strong>{{ $errors->first('suburb') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group g-mb-25">
                    <label >*What services will you provide?</label>
                    <select name="service" class="form-control rounded-0" required>
                        <option {{ !old('service') ? 'selected' : '' }} disabled value="">Choose an Option</option>
                        <option {{ old('service') == 'Pickup' ? 'selected' : '' }} value="Pickup">Pickup</option>
                        <option {{ old('service') == 'Delivery' ? 'selected' : '' }} value="Delivery">Delivery</option>
                        <option {{ old('service') == 'Pickup & Delivery' ? 'selected' : '' }} value="Pickup & Delivery">Both</option>
                    </select>
                    @if ($errors->has('service'))
                        <div class="help-block">
                            <strong>{{ $errors->first('service') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group g-mb-25">
                    <label >*What describes your business the best?</label>
                    <select name="type" class="form-control rounded-0" required>
                        <option {{ !old('type') ? 'selected' : '' }} disabled value="">Choose an Option</option>
                        <option {{ old('type') == 'Restaurant' ? 'selected' : '' }} value="Restaurant">Restaurant</option>
                        <option {{ old('type') == 'Cafe' ? 'selected' : '' }} value="Cafe">Cafe</option>
                        <option {{ old('type') == 'FastFood' ? 'selected' : '' }} value="FastFood">Fast Food</option>
                        <option {{ old('type') == 'Other' ? 'selected' : '' }} value="Other">Other</option>
                    </select>
                    @if ($errors->has('type'))
                        <div class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group g-mb-25">
                    <label >*What is your Venue’s website (or social media links)?</label>
                    <input type="text"  name="website" class="form-control rounded-0 form-control-md" placeholder="Enter business link" value="{{ old('website') }}" required>
                    @if ($errors->has('website'))
                        <div class="help-block">
                            <strong>{{ $errors->first('website') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group g-mb-25">
                    <label >*What is your role in the business?</label>
                    <select name="role" class="form-control rounded-0" required>
                        <option {{ !old('role') ? 'selected' : '' }} disabled value="">Choose an Option</option>
                        <option value="Owner" {{ old('role') == 'Owner' ? 'selected' : '' }}>Owner</option>
                        <option value="Chef" {{ old('role') == 'Chef' ? 'selected' : '' }}>Chef</option>
                        <option value="Manager" {{ old('role') == 'Manager' ? 'selected' : '' }}>Manager</option>
                        <option value="Staff" {{ old('role') == 'Staff' ? 'selected' : '' }}>Staff</option>
                    </select>
                    @if ($errors->has('role'))
                        <div class="help-block">
                            <strong>{{ $errors->first('role') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group g-mb-25">
                    <label >*How did you know about One Corner?  </label>
                    <select name="source" class="form-control rounded-0" required>
                        <option {{ !old('source') ? 'selected' : '' }} disabled value="">Choose an Option</option>
                        <option {{ old('source') == 'Saw it in action at a venue' ? 'selected' : '' }}>Saw it in action at a venue.</option>
                        <option {{ old('source') == 'One Corner Social Media' ? 'selected' : '' }}>One Corner Social Media</option>
                        <option {{ old('source') == 'Hospos Groups on Social Media' ? 'selected' : '' }}>Hospos Groups on Social Media</option>
                        <option {{ old('source') == 'Social Media Posts by other venues' ? 'selected' : '' }}>Social Media Posts by other venues</option>
                        <option {{ old('source') == 'Google Search' ? 'selected' : '' }}>Google Search</option>
                        <option {{ old('source') == 'Word of Mouth' ? 'selected' : '' }}>Word of Mouth</option>
                        <option {{ old('source') == 'Other' ? 'selected' : '' }}>Other</option>
                    </select>
                    @if ($errors->has('source'))
                        <div class="help-block">
                            <strong>{{ $errors->first('source') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group g-mb-25">
                    <label >Are you currently using any other online ordering system? If yes, please mention.</label>
                    <input type="text"  name="current_system" class="form-control rounded-0 form-control-md" placeholder="Current System" value="{{ old('current_system') }}">
                    @if ($errors->has('current_system'))
                        <div class="help-block">
                            <strong>{{ $errors->first('current_system') }}</strong>
                        </div>
                    @endif
                </div>
            </div>  
        </div>
        <input type="submit" class="btn btn-lg btn-primary" value="Apply Now">
    
    </form>
</div>
@endsection