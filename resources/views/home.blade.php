@extends('layouts.app')

@section('body')
       <style>
          .overlay{
          background-color:#00000060;
        }
        .card_btn{
          position: absolute;
            bottom: -19px;
            right: 27px;
            font-size: 14px;
        }
        
        </style>
      <div>
        <div id="homebg" class=" g-pos-rel g-bg-primary ng-scope " style="height:100%;">
        <div class="" style="height: 100% ">
            <div class="u-bg-overlay__inner text-center g-px-50" style="padding-top:10%">
             <div class="row">
               <div class="col-md-6">

                  <h2 class="g-color-white g-font-size-40 g-font-weight-600 g-mt-50">UNLIMITED ORDERS</h2>
                  <h2  class="g-color-white g-font-size-40 g-font-weight-600">NO COMMISSION</h2>
                  <p class="g-color-white g-font-size-30 g-font-weight-500">Online Order Management System</p>
                  <div style="display: inline">
                      <a href="https://play.google.com/store/apps/details?id=com.onecorner.orderapp" target="_blank">
                        <img id="playstore-img" src="/web/assets/img/playstore.png" style="height:4.5rem ; margin-bottom: 0.25rem;margin-left:-12px;">
                      </a>
                      <a data-modal-target="#appStoreModal" data-modal-effect="fadein" href="#!">
                        <img  id="playstore-img" src="/web/assets/img/appstore.png" style="height: 42px;
                        margin-bottom: 0.25rem;
                        border-radius: 8px;">
                      </a>
                  </div><br><br>  
                  <a class="btn btn-lg text-uppercase u-btn-blue g-font-weight-700 g-rounded-10 g-px-13 g-py-10 mb-0" href="/apply">Venue Apply Now</a>
               </div>
               <div class="col-md-6 hidden-sm">
                 <img class="" src="/web/assets/img/banner_img.png" style="height:35rem;" />
               </div>
              </div>
            </div>

          </div>
        <svg class="g-pos-abs g-bottom-0" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1921 183.5" enable-background="new 0 0 1921 183.5" xml:space="preserve">
          <path fill="#fff" d="M0,183.5v-142c0,0,507,171,1171,58c0,0,497-93,750,84H0z"></path>
          <path opacity="0.2" fill="#FFFFFF" d="M0,183V0c0,0,507,220.4,1171,74.7c0,0,497-119.9,750,108.3H0z"></path>
        </svg>

      </div>  

      <!-- Section Content -->
      <section id="about">
        <div class="container g-py-30">
            <div class="row">
              <div class="col-md-4">
                <!-- Icon Blocks -->
                <article class="media g-mb-20">
                  <div class="d-flex align-self-center mr-4">
                    <img src="/web/assets/img/order_ahead.png" alt="Order Ahead"/>
                  </div>
                  <div class="media-body align-self-center">
                    <h3 class="h4 g-color-black">Order Ahead</h3>
                  </div>
                </article>
                <!-- End Icon Blocks -->
              </div>
               <div class="col-md-4">
                <!-- Icon Blocks -->
                <article class="media g-mb-20">
                  <div class="d-flex align-self-center mr-4">
                    <img src="/web/assets/img/pickup.png" alt="Order Ahead"/>
                  </div>
                  <div class="media-body align-self-center">
                    <h3 class="h4 g-color-black">Pickup</h3>
                  </div>
                </article>
                <!-- End Icon Blocks -->
              </div>
               <div class="col-md-4">
                <!-- Icon Blocks -->
                <article class="media g-mb-20">
                  <div class="d-flex align-self-center mr-4">
                    <img src="/web/assets/img/delivery.png" alt="Order Ahead"/>
                  </div>
                  <div class="media-body align-self-center">
                    <h3 class="h4 g-color-black">Delivery</h3>
                  </div>
                </article>
                <!-- End Icon Blocks -->
              </div>
            </div>
        </div>
      </section>
      <!-- End Section Content -->

      <!-- Section Content -->
      <section class="g-theme-bg-gray-light-v2 g-py-30">
        <div class="text-center g-mb-60">
          <div class="container g-max-width-770">
            <h2 class="g-line-height-1 g-font-weight-700 g-font-size-30 g-color-gray-dark-v1 g-mb-30">Why Us</h2>

            <p class="mb-0 g-color-black">One Corner understands the needs and challenges of the hospitality industry. With aggregators charging you for thriving, it’s now time for you to regain control.
            </p>
          </div>
        </div>
      </section>

      <section class="g-flex-centered g-bg-white g-pb-90">
          <div class="container">
          <h2 class="text-center g-line-height-1 g-font-weight-700 g-font-size-30 g-color-gray-dark-v1 g-mb-30">Take Control of your Orders</h2>

            <div class="row">
              <div class="col-md-6 align-self-center text-center g-py-20">
                  <img src="/web/assets/img/why_us.png" style="width:20rem" />
              </div>

              <div class="col-md-6 align-self-center g-py-20">
                <h2 class="h4 g-font-weight-600 g-letter-spacing-1 g-mb-20">Unlimited Orders . No Commission</h2>
                <p class="g-color-black mb-0 g-line-height-2 g-mb-20" style="text-align: justify">
                    Don’t let  third party integration kill your business. Say no to commission for any order ever again. Our pricing is simple - pay only for monthly subscription with no hidden commission per order.
                </p>
                <a  href="#!" onclick="document.getElementById('feautre_nav_link').click()">Learn More ></a>
              </div>
              
            </div>
            <div class="row">
              

              <div class="col-md-6 align-self-center g-py-20">
                <h2 class="h4 g-font-weight-600 g-letter-spacing-1 g-mb-20">Your Brand . Your Design</h2>
                <p class="g-color-black mb-0 g-line-height-2 g-mb-20" style="text-align: justify">
                    You can customize our order management system to integrate with your brand. With our user friendly menu design and check out process your customer can easily navigate and place an order.
                  </p>
                <a  href="#!" onclick="document.getElementById('feautre_nav_link').click()">Learn More ></a>


              </div>
              <div class="col-md-6 align-self-center text-center g-py-20">
                  <img src="/web/assets/img/your_logo.png" style="max-width:100%"  />
              </div>
              
            </div>
            <div class="row">
              <div class="col-md-6 align-self-center text-center g-py-20">
                  <img src="/web/assets/img/phone.png" style="max-width:20rem" />
              </div>

              <div class="col-md-6 align-self-center g-py-20">
                <h2 class="h4 g-font-weight-600 g-letter-spacing-1 g-mb-20">Manage Your Orders</h2>
                <p class="g-color-black mb-0 g-line-height-2 g-mb-20" style="text-align: justify">
                    Our easy and friendly dashboard makes it easier for you to receive and accept new order, create and update menu, gain meaningful insight through order history, 
                    change prices, set and update delivery areas, offer promotions, set and update opening 
                    hours.
                </p>
                <a  href="/apply" >Get your online ordering system ></a>

              </div>
              
            </div>
          </div>
      </section>
      <!-- End Section Content -->


      <!-- Section Content -->
      <section id="features" class="">
        <div class="text-center g-mb-60">
          <div class="container g-max-width-770">
            <h2 class= "g-line-height-1 g-font-weight-700 g-font-size-30 g-color-gray-dark-v1 g-mb-30">Features</h2>

            <p class="mb-0 g-color-black g-font-weight-500 g-font-size-20">
                Discover what makes us <span class="g-color-primary">ONE CORNER</span> for your restaurant.
            </p>
          </div>
        </div>

        <div class="container ">
            <div class="row">
                <div class="col-lg-4 g-mb-40">
                  <div class="text-center u-icon-block--hover">
                    <img class="" style="height:12rem" src="/web/assets/img/icon_feature_1.png" alt="Image Description">
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">24/7 Real-time</h3>
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Online Orders</h3>
                  </div>
                </div>
                <div class="col-lg-4 g-mb-40">
                  <div class="text-center u-icon-block--hover">
                    <img class="" style="height:12rem" src="/web/assets/img/icon_feature_2.png" alt="Image Description">
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Pickup and</h3>
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Delivery Service</h3>
                  </div>
                </div>
                <div class="col-lg-4 g-mb-40">
                  <div class="text-center u-icon-block--hover">
                    <img class="" style="height:12rem" src="/web/assets/img/icon_feature_3.png" alt="Image Description">
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Online Credit</h3>
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Payment System</h3>
                  </div>
                </div>
                <div class="col-lg-4 g-mb-40">
                  <div class="text-center u-icon-block--hover">
                    <img class="" style="height:12rem" src="/web/assets/img/icon_feature_4.png" alt="Image Description">
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Multiple Venues</h3>
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Multiple Users</h3>
                  </div>
                </div>
                <div class="col-lg-4 g-mb-40">
                  <div class="text-center u-icon-block--hover">
                    <img class="" style="height:12rem" src="/web/assets/img/icon_feature_5.png" alt="Image Description">
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Customers Pay</h3>
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Directly to You</h3>
                  </div>
                </div>
                <div class="col-lg-4 g-mb-40">
                  <div class="text-center u-icon-block--hover">
                    <img class="" style="height:12rem" src="/web/assets/img/icon_feature_6.png" alt="Image Description">
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Quick and</h3>
                    <h3 class="h5 g-color-black g-font-weight-600 mb-0">Easy Setup</h3>
                  </div>
                </div>
              
              </div>
           </div>
        </div>
       
      </section>
      <div id="contact" class=" g-pos-rel g-bg-primary ng-scope " style="height:100%;">
          <div class="" style="height: 100% ;">
              <svg style="  transform: rotate(180deg);" class="g-pos-abs g-top-0" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1921 183.5" enable-background="new 0 0 1921 183.5" xml:space="preserve">
                <path fill="#fff" d="M0,183.5v-142c0,0,507,171,1171,58c0,0,497-93,750,84H0z"></path>
                <path opacity="0.2" fill="#FFFFFF" d="M0,183V0c0,0,507,220.4,1171,74.7c0,0,497-119.9,750,108.3H0z"></path>
              </svg>
              <div class="text-center container " style="padding-top:10%">
                <p class="g-color-white g-font-size-33">Lets get connected</p>
               <div class="row mt-30">
                  <div class="col-md-6">
                      <div class="u-shadow-v19 g-bg-white g-pa-30 g-mt-30" style="max-width:40rem">
                          <div class="media g-mb-15">
                            <div class="media-body">
                              <p class="g-color-black g-mb-0 text-left">
                                  Want to partner with One Corner and <br>
                                  Reach more customers?
                                  
                              </p>
                              <a  class="btn btn-lg u-btn-blue g-font-weight-700 g-rounded-10 g-px-13 g-py-10 mb-0 card_btn" href="/apply">Venue Apply Now</a>

                            </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="u-shadow-v19 g-bg-white g-pa-30 g-mt-30" style="max-width:40rem">
                          <div class="media g-mb-15">
                            <div class="media-body">
                              <p class="g-color-black g-mb-0 text-left">
                                  Want to get a look of the online ordering <br>
                                  system? Request a Demo Now.
                                  
                              </p>
                              <a data-modal-target="#bookDemoModal" data-modal-effect="fadein"  class="btn btn-lg u-btn-blue g-font-weight-700 g-rounded-10 g-px-13 g-py-10 mb-0 card_btn" href="#!">Request a Demo</a>

                            </div>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
              <div class="g-pb-1">
              <p class="text-center g-color-white g-mt-40 g-mb-4 g-font-size-18"><a style="color:white" href="mailto:contact@onecorner.com.au" target="blank"><i class="fa fa-envelope"></i> contact@onecorner.com.au </a><br></p>
              </div>
            </div>
            <div id="bookDemoModal" class="text-left g-bg-white g-pa-20" style="display: none;overflow-y: scroll;">
                <button type="button" class="close" style="cursor:pointer" onclick="Custombox.modal.close();">
                  <i class="hs-icon hs-icon-close"></i>
                </button>
                <h4 class="g-mb-20">
                    Reuqest a Demo
                    </h4>
                <p>
                    Please fill up this quick form and we will be in contact with you ASAP.
                </p>
                <form class="g-brd-around g-brd-gray-light-v4 g-pa-30"  method="POST" action="{{ route('contact') }}">
                  @csrf

                    <div class="form-group g-mb-25">
                      <label>*Name</label>
                      <input type="text" name="name" class="form-control rounded-0 form-control-md" value="{{ old('name') }}"  placeholder="Enter Name" required>
                      @if ($errors->has('name'))
                          <div class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </div>
                      @endif
                    </div>
                    <div class="form-group g-mb-25">
                      <label>*Email</label>
                      <input type="email" name="email" class="form-control rounded-0 form-control-md" value="{{ old('email') }}" placeholder="Enter email" required>
                      @if ($errors->has('email'))
                          <div class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </div>
                      @endif
                    </div>
                    <div class="form-group g-mb-25">
                      <label>*Phone Number</label>
                      <input type="text" name="contact" class="form-control rounded-0 form-control-md" value="{{ old('contact') }}" placeholder="Enter phone number" required>
                      @if ($errors->has('contact'))
                          <div class="help-block">
                              <strong>{{ $errors->first('contact') }}</strong>
                          </div>
                      @endif
                    </div>
                    <div class="form-group g-mb-25">
                      <label>*Business Name</label>
                      <input type="text" name="businessName" class="form-control rounded-0 form-control-md" value="{{ old('businessName') }}" placeholder="Enter business name" required>
                      @if ($errors->has('businessName'))
                          <div class="help-block">
                              <strong>{{ $errors->first('businessName') }}</strong>
                          </div>
                      @endif
                    </div>
                    <div class="form-group g-mb-0" style="text-align:right">
                      <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                    </div>
                </form>
           
             </div>
             
           
  
        </div>  
  
  @endsection

@section('js')
  @if(count($errors) > 0){
    <script>
        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#bookDemoModal'
            }
        });
    
        // Open
        modal.open();
    </script>
  @endif

@endsection