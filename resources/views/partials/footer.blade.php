
      <footer class="container g-pt-20 g-pb-20">
          <div class="row">
            <div class="col-md-3 text-center text-md-left g-mb-0">
              <a class="d-inline-block g-text-underline--none--hover mr-4" href="/">
                <img class="" style="height:5rem" src="/web/assets/img/Logo.png" alt="Logo">
              </a>
              <span class="d-block g-color-black g-font-size-13">© One Corner 2020. All Rights Reserved.</span>

            </div>
        
            <div class="col-md-6 text-center g-mb-0">
        
              <!-- Footer - List -->
              <ul class="list-inline mt-4">
                  <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v2 g-width-35 g-height-35 g-brd-gray-light-v3 g-brd-primary--hover g-color-black g-color-white--hover g-bg-transparent g-bg-primary--hover g-font-size-20" target="_blank" href="https://www.facebook.com/onecornerau/">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-2">
                      <a class="u-icon-v2 g-width-35 g-height-35 g-brd-gray-light-v3 g-brd-primary--hover g-color-black g-color-white--hover g-bg-transparent g-bg-primary--hover g-font-size-20" target="_blank" href="https://www.instagram.com/onecornerau/">
                        <i class="fa fa-instagram"></i>
                      </a>
                    </li>
                  <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v2 g-width-35 g-height-35 g-brd-gray-light-v3 g-brd-primary--hover g-color-black g-color-white--hover g-bg-transparent g-bg-primary--hover g-font-size-20" target="_blank" href="https://twitter.com/onecornerau">
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  
                </ul>
              <!-- End Footer - List -->
        
            </div>
        
            <div class="col-md-3 text-center g-mb-  0">
                <ul class="list-inline">
                    <li class="list-inline-item mb-1">
                      <a href="https://play.google.com/store/apps/details?id=com.onecorner.orderapp" target="_blank">
                        <img id="playstore-img" src="/web/assets/img/playstore.png" style="height:4.5rem ; margin-bottom: 0.25rem;margin-left:-12px;">
                      </a><br>  
                    </li>
                    
                    <li class="list-inline-item mb-1">
                      <a data-modal-target="#appStoreModal" data-modal-effect="fadein"  href="#!" style="margin-bottom:0.7rem;margin-left: -14px;">
                        <img id="appstore-img" src="/web/assets/img/appstore.png" style="height:3rem; border-radius:7px"> 
                      </a>
                    </li>
                  
                  </ul>
        
              <!-- Social Icons -->
             
              <!-- End Social Icons -->
            </div>
          </div>
        </footer>
      
    </main>
    <div id="successModal" class="text-left g-max-width-600 g-bg-primary g-overflow-y-auto g-pa-20" style="display: none; max-width:1000px !important">
      <button type="button" class="close" onclick="Custombox.modal.close();">
        <i class="hs-icon hs-icon-close"></i>
      </button>
      
      <div class="container">
            <div class="text-center">
                <h4 class="g-mb-20">Thank You!</h4>
                <p><strong id="successMessage" style="color:white">{{ Session::get('success') }}</strong></p>
               <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
            </div>
       </div>
    </div>
    <div id="appStoreModal" class="text-left g-bg-white g-overflow-y-auto g-pa-20" style="display: none;">
      <button type="button" class="close" style="cursor:pointer" onclick="Custombox.modal.close();">
        <i class="hs-icon hs-icon-close"></i>
      </button>
      <h4 class="g-mb-20">
          Coming Soon..
      </h4>
      <p>
          Our App will be available for iOS by April 25th.
      </p>
 
    </div>

    <script src="/web/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>

    <script src="/web/frontend-assets/main-assets/assets/vendor/appear.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/vendor/fancybox/jquery.fancybox.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/vendor/cubeportfolio-full/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>

    <script src="/web/frontend-assets/main-assets/assets/js/hs.core.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/js/components/hs.header.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/js/components/hs.scroll-nav.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/js/components/hs.onscroll-animation.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/js/components/hs.tabs.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/js/components/hs.cubeportfolio.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/js/components/hs.popup.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/js/components/hs.carousel.js"></script>
    <script src="/web/frontend-assets/main-assets/assets/js/components/hs.go-to.js"></script>
    <script type="text/javascript" src="/web/frontend-assets/main-assets/assets/vendor/page-scroll-to-id/jquery.malihu.PageScroll2id.js"></script>
       <!-- JS Implementing Plugins -->
       <script  src="/web/frontend-assets/main-assets/assets/vendor/custombox/custombox.min.js"></script>

       <!-- JS Unify -->
       <script  src="/web/frontend-assets/main-assets/assets/js/components/hs.modal-window.js"></script>

       <!-- JS Plugins Init. -->
       <script >
         $(document).on('ready', function () {
           // initialization of popups
           $.HSCore.components.HSModalWindow.init('[data-modal-target]');
         });
       </script>

    <!-- JS Customization -->
    <script src="/web/frontend-assets/main-assets/assets/js/custom.js"></script>
      @yield('js')
      @if(Session::has('success'))
        <script>
            var modal = new Custombox.modal({
                content: {
                    effect: 'fadein',
                    target: '#successModal'
                }
            });
        
            // Open
            modal.open();
        </script>
        
      @endif
    <!-- JS Plugins Init. -->
    <script>
      $(document).on('ready', function () {
        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of scroll animation
        $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

        // initialization of go to section
        $.HSCore.components.HSGoTo.init('.js-go-to');

        // initialization of popups
        $.HSCore.components.HSPopup.init('.js-fancybox-media', {
          helpers: {
            media: {},
            overlay: {
              css: {
                'background': 'rgba(255, 255, 255, .8)'
              }
            }
          }
        });
      });

      $(window).on('load', function() {
        // initialization of HSScrollNav
        $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
          duration: 700
        });

        // initialization of cubeportfolio
        $.HSCore.components.HSCubeportfolio.init('.cbp');
      });

      $(window).on('resize', function () {
        setTimeout(function () {
          $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
      });
      
    </script>
  </body>
</html>
