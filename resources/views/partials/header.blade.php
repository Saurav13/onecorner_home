<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title>One Corner</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="IE=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/web/frontend-assets/main-assets/favicon.ico">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/icon-line/css/simple-line-icons.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/animate.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/cubeportfolio-full/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.css">
    <link  rel="stylesheet" href="/web/frontend-assets/main-assets/assets/vendor/custombox/custombox.min.css">

    <!-- CSS Template -->
    <link rel="stylesheet" href="/web/frontend-assets/assets/css/styles.op-app.css">
    <link rel="stylesheet" href="/web/css/responsive.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="/web/frontend-assets/main-assets/assets/css/custom.css">
    <style>
      .help-block{
        font-size: 13px;
        color: #e65454;
      }
    </style>
  </head>
