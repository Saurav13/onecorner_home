<body>
  <main>
    <header id="js-header" class="u-header u-header--sticky-top u-header--show-hide u-header--toggle-section"
            data-header-fix-moment="100"
            data-header-fix-effect="slide">
        <div class="u-header__section u-shadow-v27 g-bg-white g-transition-0_3 g-py-12 g-py-15--md">
          <nav class="navbar navbar-expand-lg py-0 g-px-15">
            <div class="container g-pos-rel">
              <a href="/" class="{{ Request::is('/') ? 'js-go-to':'' }} navbar-brand u-header__logo"
                data-type="web">
                <img class="u-header__logo-img u-header__logo-img--main" style="height:4rem" src="/web/assets/img/Logo.png" alt="Image description">
              </a>
              
              <div id="navBar" class="collapse navbar-collapse">
                <div class="navbar-collapse align-items-center flex-sm-row">
                  <ul id="js-scroll-nav" class="navbar-nav g-flex-right--xs text-uppercase w-100 g-font-weight-700 g-font-size-16  g-pt-20 g-pt-0--lg">
                    @if(Request::is('/'))
                      <li class="nav-item g-mx-15--lg g-mb-12 g-mb-0--lg">
                        <a href="#about" class="scrollpage _mPS2id-h nav-link p-0">About Us</a>
                      </li>
                      <li class="nav-item g-mx-15--lg g-mb-12 g-mb-0--lg">
                        <a href="#features" id="feautre_nav_link" class="scrollpage _mPS2id-h nav-link p-0">Features</a>
                      </li>
                      <li class="nav-item g-ml-15--lg g-mb-12 g-mb-0--lg">
                        <a href="#contact" class="scrollpage _mPS2id-h nav-link p-0">Contact</a>
                      </li>
                    @else
                      <li class="nav-item g-mx-15--lg g-mb-12 g-mb-0--lg">
                        <a href="{{URL::to('/#about')}}" class="nav-link p-0">About Us</a>
                      </li>
                      <li class="nav-item g-mx-15--lg g-mb-12 g-mb-0--lg">
                        <a href="{{URL::to('/#features')}}" class="nav-link p-0">Features</a>
                      </li>
                      <li class="nav-item g-ml-15--lg g-mb-12 g-mb-0--lg">
                        <a href="{{URL::to('/#contact')}}" class="nav-link p-0">Contact</a>
                      </li>
                    @endif
                  </ul>
                </div>
                <a data-modal-target="#bookDemoModal" data-modal-effect="fadein" class="btn btn-lg text-uppercase u-btn-primary g-font-weight-700 g-font-size-11 g-rounded-10 g-px-13 g-py-10 mb-0 g-ml-25--lg" href="/#contact">Get Demo</a>
              </div>

              <button class="navbar-toggler btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-15 g-right-0" type="button"
                      aria-label="Toggle navigation"
                      aria-expanded="false"
                      aria-controls="navBar"
                      data-toggle="collapse"
                      data-target="#navBar">
                <span class="hamburger hamburger--slider">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
              </button>
            </div>
          </nav>
        </div>
    </header>